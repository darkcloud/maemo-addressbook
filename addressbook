#!/usr/bin/env python
#
# Please let me know if this works without python-gnome2 installed!
#
# Thanks to Nicolai, DocScrutinizer, luf and everyone else on #maemo
#

VERSION = '0.1'
EXAMPLES = """
Examples:
  # To list all contacts in YAML format:
  addressbook -l

  # To search for John using the "search" feature of EDS:
  addressbook -l -s john

  # To list only contacts with specified properties in VCARD format:
  addressbook -l -p given-name=John -p family-name=Doe -f VCARD

  # To add a contact with the specified properties:
  addressbook -a -p given-name=John -p family-name=Doe -p mobile-phone=021123456 -p email-1=john@example.com

  # To list supported properties:
  addressbook -P

  # To remove contacts with the specified properties ( all that match will be removed):
  addressbook -r -p given-name=John -p family-name=Doe
"""

import os
import sys
import evolution

from optparse import OptionParser


def quit( message):
  sys.stderr.write('%s\n'% message)
  exit( 1)


if 0 == os.getuid():
  quit('Not as root please')


addressbook = evolution.ebook.open_addressbook('default')


def list_contacts( options):

  def print_contact( contact):

    if 'VCARD' == options.format:
      print contact.get_vcard_string()

    elif 'YAML' == options.format:
      first_line = True
      for ppt in contact.props:
        value = contact.get_property( ppt.name)
        if value != None and isinstance( value, str) and value != '':
          prefix = '-' if first_line else ' '
          print '%s %s: %s'% ( prefix, ppt.name, value)
          first_line = False

  for ct in contacts( options):
    print_contact( ct)


def add_contact( options):

  ct = evolution.ebook.EContact()

  for name, value in options.properties.iteritems():
    ct.set_property( name, value)

  addressbook.add_contact( ct)


def remove_contacts( options):
  for ct in contacts(options):
    addressbook.remove_contact( ct)


def contacts( options):
  if options.search:
    contacts = addressbook.search( options.search)
  else:
    contacts = addressbook.get_all_contacts()
    if options.properties != {}:
      selected = lambda ct: [ ct.get_property(name) == value for name, value in options.properties.iteritems()].count(True) == len( options.properties)
      contacts = filter( selected, contacts )
  return contacts


def list_properties():
  ct = evolution.ebook.EContact()
  ppts = [ p.name for p in ct.props]
  ppts.sort()
  print '\n'.join( ppts)


def main():
    ps = OptionParser( version=VERSION, add_help_option=False) # add_help_option=False because epilog mangles EXAMPLES
    ps.add_option('-l', '--list',     help='list [matching] contacts',              action='store_true', dest='list_contacts',   default=False)
    ps.add_option('-a', '--add',      help='add a contact',                         action='store_true', dest='add_contact',     default=False)
    ps.add_option('-r', '--remove',   help='remove matching contacts',              action='store_true', dest='remove_contacts', default=False)
    ps.add_option('-f', '--format',   help='output format: VCARD|YAML',             action='store',                              default='YAML', metavar='FMT', choices=['VCARD','YAML'])
    ps.add_option('-p', '--property', help='-p given-name=John -p family-name=Doe', action='append',     dest='properties',      default=[],     metavar='PROPDEF')
    ps.add_option('-s', '--search',   help='search for NAME not properties',        action='store',                                              metavar='NAME')
    ps.add_option('-P', '',           help='list supported properties',             action='store_true', dest='list_properties', default=False)
    ps.add_option('-h', '--help',     help='shows options and examples',            action='store_true',                         default=False)

    options, args = ps.parse_args( sys.argv)
    # Convert the list of property specifications to a map:
    properties = {}
    for ppt in options.properties:
      name, value = ppt.split('=')
      properties[ name] = value
    options.properties = properties

    if options.list_contacts:
      list_contacts( options)

    elif options.add_contact:
      add_contact( options)

    elif options.remove_contacts:
      if 0 == len( options.properties):
        quit('Whoa! This would remove ALL contacts!')
      remove_contacts( options)

    elif options.list_properties:
      list_properties()
    else:
      ps.print_help()
      print EXAMPLES

main()

